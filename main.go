package main

import (
	"fmt"
	"net/http"

	"gitlab.com/g.van.weelden/image_proxy/router"
)

func main() {
	http.Handle("/", router.Router())
	http.ListenAndServe(fmt.Sprintf(":%d", 8080), nil)
}
